import os
import time

import yaml

from utils import create_logger


def main(main_settings, log_settings):
    logger = create_logger(**log_settings)
    base_interval, reboot_path = main_settings.values()
    logger.info("start main loop")
    while True:
        if os.path.exists(reboot_path):
            logger.info("rebooting")
            os.remove(reboot_path)
            os.system("shutdown -r -f")
        time.sleep(base_interval)


if __name__ == '__main__':
    with open(r".\config.yml") as config_file:
        main(**yaml.safe_load(config_file))
