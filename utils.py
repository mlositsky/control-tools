import logging
import os
from datetime import datetime


def create_logger(name, main_level, file_level, console_level, log_dir):
    logger = logging.getLogger(name)
    logger.setLevel(main_level)
    log_name = f"{name}-{dt_iso_safe()}.log"
    file_handler = logging.FileHandler(f"{os.path.join(log_dir, log_name)}")
    file_handler.setLevel(file_level)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_level)
    formatter = logging.Formatter("[ %(asctime)s ] [ %(levelname)s ] %(message)s")
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    return logger


def dt_iso_safe():
    return datetime.now().isoformat().replace("-", "").replace(":", "")[:-7]
