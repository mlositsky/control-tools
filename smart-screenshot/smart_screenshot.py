from utils import create_logger, dt_iso_safe


def main(main_settings, log_settings):
    base_interval, screenshot_dir = main_settings.values()
    logger = create_logger(**log_settings)
    previous_screenshot_path = None
    logger.info("start main loop")
    time.sleep(30)
    while True:
        screenshot_name = f"{dt_iso_safe()}.png"
        screenshot_path = f"{os.path.join(screenshot_dir, screenshot_name)}"
        try:
            pyautogui.screenshot(screenshot_path)
            if not previous_screenshot_path or md5_diff(
                previous_screenshot_path, screenshot_path
            ):
                previous_screenshot_path = screenshot_path
                logger.info(f"{screenshot_path} saved")
            else:
                logger.info("nothing happens")
                os.remove(screenshot_path)
        except OSError as error:
            logger.error(f"{str(error)}")

        time.sleep(int(base_interval))


def md5_diff(path_a, path_b):
    with open(path_a, "rb") as file_a:
        contents_a = file_a.read()
    with open(path_b, "rb") as file_b:
        contents_b = file_b.read()
    hash_a = hashlib.md5(contents_a).hexdigest()
    hash_b = hashlib.md5(contents_b).hexdigest()
    if hash_a == hash_b:
        return False
    return True


if __name__ == "__main__":
    try:
        import os
        import time
        import yaml
        import pyautogui
        import hashlib
        import traceback

        with open(r".\config.yml") as config_file:
            main(**yaml.safe_load(config_file))
    except Exception as e:
        import ctypes

        ctypes.windll.user32.MessageBoxW(
            0, f"{str(e)}", "smart-screenshot exception", 1
        )
        raise e
